﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using RandomCriterion.Model;

namespace DistributionResearch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private MT19937 RandomMT = new MT19937();
        private int count = 1000;

        private void button1_Click(object sender, EventArgs e)
        {
            // RandomMT.RandomRange(low,high)
            var a = Convert.ToDouble(numericUpDown1.Text);
            var b = Convert.ToDouble(numericUpDown2.Text);
            var left = Convert.ToDouble(numericUpDown3.Text);
            var right = Convert.ToDouble(numericUpDown4.Text);
            count = Convert.ToInt32(numericUpDown9.Text);

            List<DataPoint> points = new List<DataPoint>();

            for (int i = 0; i < count; i++)
            {
                double x = GetUniform(a, b);
                points.Add(new DataPoint(x, 1/(b - a)));
                
            }

            points = points.OrderBy(p => p.XValue).ToList();
            AddPointsTo(0,points,left,right);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var m = Convert.ToDouble(numericUpDown5.Text);
            var s2 = Convert.ToDouble(numericUpDown6.Text);
            var left = Convert.ToDouble(numericUpDown7.Text);
            var right = Convert.ToDouble(numericUpDown8.Text);
            count = Convert.ToInt32(numericUpDown9.Text);

            List<DataPoint> points = new List<DataPoint>();

            for (int i = 0; i < count; i++)
            {
                double x = GetNormal2(m, Math.Sqrt(s2)); //GetNormal2(m, Math.Sqrt(s2));
                while (x<left || x>right)
                {
                    x = GetNormal2(m, Math.Sqrt(s2));
                }
                double y = 1/(Math.Sqrt(s2*2*Math.PI))*Math.Exp(-Math.Pow(x - m, 2)/(2*s2));
                points.Add(new DataPoint(x,y));
            }

            points = points.OrderBy(p => p.XValue).ToList();
            AddPointsTo(1, points, left, right);  
            
        }

        private void AddPointsTo(int chartArea, List<DataPoint> points, double left, double right)
        {
            chart.Series[chartArea].Points.Clear();
            foreach (var pointD in points)
            {
                chart.Series[chartArea].Points.Add(pointD);
            }
            chart.ChartAreas[chartArea].AxisX.Maximum = right;
            chart.ChartAreas[chartArea].AxisX.Minimum = left;
            chart.ChartAreas[chartArea].AxisX.RoundAxisValues();
        }

        public double GetUniform(double a, double b)
        {
            return a + RandomMT.genrand_real1() * (b - a);
        }
        
        private double GetNormal2(double mean, double standardDeviation)
        {
            return mean + standardDeviation * GetNormal();
        }

        // Get normal (Gaussian) random sample with mean 0 and standard deviation 1
        public double GetNormal()
        {
            // Use Box-Muller algorithm https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%B5%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D0%91%D0%BE%D0%BA%D1%81%D0%B0_%E2%80%94_%D0%9C%D1%8E%D0%BB%D0%BB%D0%B5%D1%80%D0%B0 
            double u1 = RandomMT.genrand_real1();
            double u2 = RandomMT.genrand_real1();
            double r = Math.Sqrt(-2.0 * Math.Log(u1));
            double theta = 2.0 * Math.PI * u2;
            return r * Math.Sin(theta);
        }
    }
}
